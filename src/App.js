import React, { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./Components/Login/Login";
import PrivateRoot from "./Components/PrivateRoot";
import Home from "./Components/Home/Home";
import Leaderboard from "./Components/Leaderboard/Leaderboard";
import New from "./Components/New/New";
import DetailQuestion from "./Components/Home/DetailQuestion/DetailQuestion";

export default function App() {
  const [id, Setid] = useState("");
  const [avatarURL, SetavatarURL] = useState({});

  useEffect(() => {
    Setid(localStorage.getItem("id"));
    SetavatarURL(localStorage.getItem("avatarURL"));
  });
  return (
    <div>
      <BrowserRouter>
        <div className="d-flex justify-content-space margin">
          <ul className="nav">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="/">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="leaderboard">
                Leaderboard
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="new">
                New
              </a>
            </li>
          </ul>
          <div className="d-flex">
            <div className="d-flex  ">
              <div className="w-img">
                <img className="img height" src={avatarURL} />
              </div>
              <span>{id}</span>
            </div>
            <li className="nav-item" style={{ listStyleType: "none" }}>
              <a className="nav-link" href="login">
                Logout
              </a>
            </li>
          </div>
        </div>
        <hr />
        <Routes>
          <Route path="login" element={<Login />} />
          <Route
            path="/"
            element={
              <PrivateRoot>
                <Home />
              </PrivateRoot>
            }
          />
          <Route
            path="detailquestion"
            element={
              <PrivateRoot>
                <DetailQuestion />
              </PrivateRoot>
            }
          />
          <Route
            path="leaderboard"
            element={
              <PrivateRoot>
                <Leaderboard />
              </PrivateRoot>
            }
          />
          <Route
            path="new"
            element={
              <PrivateRoot>
                <New />
              </PrivateRoot>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
