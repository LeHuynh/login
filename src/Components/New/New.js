import React, { useState } from "react";
import "./New.css";
import { _saveQuestion } from "../../utils/ApiData";
import { useNavigate } from "react-router-dom";

export default function New() {
  const [optionOne, setoptionOne] = useState([]);
  const [optionTwo, setoptionTwo] = useState([]);
  const nav = useNavigate();
  const handleClick = (optionOneText, optionTwoText, author) => {
    _saveQuestion({ optionOneText, optionTwoText, author });
    nav("/");
  };

  return (
    <div className="margin-auto">
      <h1 className="text-center mb-5">Would you Rather</h1>
      <div>
        <p>Fisrt Option</p>
        <input onChange={(element) => setoptionOne(element.target.value)} value={optionOne} className="input" placeholder="optionOne" />
      </div>
      <div className="mt-5 ">
        <p>Second Option</p>
        <input onChange={(element) => setoptionTwo(element.target.value)} value={optionTwo} className="input" placeholder="optionTwo" />
      </div>
      <button onClick={() => handleClick(optionOne, optionTwo, localStorage.getItem("id"))} className="mt-5 btn btn-submit px-4 py-2 rounded-pill">
        Submit
      </button>
    </div>
  );
}
