import React, { useState } from "react";
import "./Login.css";
import { _getUsers } from "../../utils/ApiData";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const [user, SetUser] = useState("tylermcginnis");
  const [password, SetPassword] = useState("abc321");
  const nav = useNavigate();

  const HandleClick = () => {
    _getUsers().then((dt) => {
      if (dt[user]?.password === password) {
        localStorage.setItem("id", user);
        localStorage.setItem("avatarURL", dt[user].avatarURL);

        console.log(dt);
        nav("/");
      }
    });
  };
  return (
    <div>
      <div className="margin-auto">
        <div>
          <h1 className="text-center">Emp loyee Polls</h1>
        </div>
        <div className=" margin-auto">
          <img className="width-img border-radius margin-auto" src={"https://toigingiuvedep.vn/wp-content/uploads/2021/04/hinh-anh-nen-con-meo-cute.jpg"} />
        </div>
        <h1 className="text-center">Log In</h1>
        <div className="margin-auto">
          <p className="text-center">User</p>
          <input placeholder="User" onChange={(e) => SetUser(e.target.value)} className="input padding" value={user} />
        </div>
        <div className="margin-auto mt-5">
          <p className="text-center">Password</p>
          <input placeholder="Password" onChange={(e) => SetPassword(e.target.value)} className="input padding" value={password} />
        </div>

        <button onClick={HandleClick} className="padding-btn bg-btn mt-5 color-white">
          Submit
        </button>
      </div>
    </div>
  );
}
