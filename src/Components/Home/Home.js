import React, { useContext, useEffect, useState } from "react";
import "./Home.css";
import { _getQuestions } from "../../utils/ApiData";
import { ctx } from "../../Context/CtxData";
import { useNavigate } from "react-router-dom";

export default function Home() {
  const [id, Setid] = useState("");
  const [avatarURL, setavatarURL] = useState({});
  const [qs, Setquestion] = useState([]);
  const [newQuestion, SetnewQuestion] = useState([]);
  const [done, Setdone] = useState([]);
  const state = useContext(ctx);
  const nav = useNavigate();

  useEffect(() => {
    console.log(localStorage.getItem("id"));
    Setid(localStorage.getItem("id"));
    setavatarURL(localStorage.getItem("avatarURL"));

    const qx = [];
    _getQuestions().then((dt) => {
      Object.keys(dt).forEach((key) => {
        console.log(qx);
        qx.push(dt[key]);
      });
      Setquestion(qx);
    });
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    let arrQuestion = qs.filter((n) => {
      let arr = [...n.optionOne.votes, ...n.optionTwo.votes].includes(id);
      //eslint-disable-next-line
      if (arr == false) {
        return n;
      }
    });
    SetnewQuestion(arrQuestion);
    let arrDone = qs.filter((n) => {
      let arr = [...n.optionOne.votes, ...n.optionTwo.votes].includes(id);

      if (arr == true) {
        return n;
      }
    });
    Setdone(arrDone);
    console.log(qs);
    //eslint-disable-next-line
  }, [qs]);
  //
  console.log(qs);
  const handleClick = (element) => {
    state.setDetailQuestion(element);
    nav("/detailquestion");
  };
  return (
    <div>
      <div className="card mx-auto text-center mt-5" style={{ width: "655px" }}>
        <div className="card-header">New Question</div>
        <ul className="list-group list-group-flush">
          <li class="list-group-item d-flex gap" style={{ flexWrap: "wrap" }}>
            {newQuestion.map((element, index) => {
              return (
                <div key={index} className="card" style={{ width: "200px" }}>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">{element.author}</li>
                    <li class="list-group-item">
                      <button onClick={() => handleClick(element)} className="border-btn color-green bg-white btn mx-auto">
                        Show
                      </button>
                    </li>
                  </ul>
                </div>
              );
            })}
          </li>
        </ul>
      </div>
      <div className="card mx-auto text-center mt-5" style={{ width: "655px" }}>
        <div className="card-header">Done</div>
        <ul className="list-group list-group-flush">
          <li class="list-group-item d-flex gap" style={{ flexWrap: "wrap" }}>
            {done.map((element, index) => {
              return (
                <div key={index} className="card" style={{ width: "200px" }}>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">{element.author}</li>
                    <li class="list-group-item">
                      <button onClick={() => handleClick(element)} className="border-btn color-green bg-white btn mx-auto">
                        Show
                      </button>
                    </li>
                  </ul>
                </div>
              );
            })}
          </li>
        </ul>
      </div>
    </div>
  );
}
