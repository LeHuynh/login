import React, { useContext } from "react";
import "./DetailQuestion.css";
import { useNavigate } from "react-router-dom";
import { ctx } from "../../../Context/CtxData";
import { _saveQuestionAnswer } from "../../../utils/ApiData";

export default function DetailQuestion() {
  const state = useContext(ctx);
  const authedUser = localStorage.getItem("id");
  const qid = state.detailQuestion.id;
  const nav = useNavigate();
  console.log(state);
  const handleOne = () => {
    _saveQuestionAnswer({ authedUser, qid, answer: "optionOne" });
    nav("/");
  };
  const handleTwo = () => {
    _saveQuestionAnswer({ authedUser, qid, answer: "optionTwo" });
    nav("/");
  };

  return (
    <div>
      <h1 className="text-center">Would You Rather</h1>
      <div className="d-flex gap-5 justify-content-center mt-5">
        <div style={{ textAlign: "center" }}>
          <p style={{ border: "1px solid black", padding: "20px" }}>{state.detailQuestion?.optionOne?.text}</p>
          <button onClick={handleOne} className="btn bt1 btn-click px-5 py-2 rounded-pill">
            Click
          </button>
        </div>

        <div className="" style={{ textAlign: "center" }}>
          <p style={{ border: "1px solid black", padding: "20px" }}>{state.detailQuestion?.optionTwo?.text}</p>
          <button onClick={handleTwo} style={{}} className="btn bt1 btn-click px-5 py-2 rounded-pill">
            Click
          </button>
        </div>
      </div>
    </div>
  );
}
