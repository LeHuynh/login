import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function PrivateRoot(props) {
  const nav = useNavigate();

  useEffect(() => {
    let id = localStorage.getItem("id");
    if (!id) {
      nav("/login");
    }
  }, []);
  return <>{props.children}</>;
}
