import React, { createContext, useState } from "react";

export const ctx = createContext();

export default function CtxData(props) {
  const [listLogin, SetlistLogin] = useState([]);
  const [detailQuestion, setDetailQuestion] = useState([]);
  return (
    <ctx.Provider
      value={{
        listLogin,
        SetlistLogin,
        detailQuestion,
        setDetailQuestion,
      }}
    >
      {props.children}
    </ctx.Provider>
  );
}
